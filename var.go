package main

import (
	//	"github.com/johnnadratowski/golang-neo4j-bolt-driver"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2"
)

var session *mgo.Session
var collection *mgo.Collection
var collectionMeta *mgo.Collection
var collectionJoin *mgo.Collection

//var conn golangNeo4jBoltDriver.Conn
//var driver golangNeo4jBoltDriver.Driver

var mom *amqp.Connection
var ch *amqp.Channel
var q amqp.Queue

var amqpPath string

var documentDBUrl string

//var graphDBUrl string = "bolt://neo4j:admin@localhost:7687"
var rabbitMQUrl string
