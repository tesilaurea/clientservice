package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
	Route{
		"ReceiveAbstractWorkflow",
		"POST",
		"/workflow/create",
		ReceiveAbstractWorkflow,
	},
	Route{
		"DeployWorkflow",
		"POST",
		"/workflow/deploy",
		DeployWorkflow,
	},
	Route{
		"GetWorkflow",
		"GET",
		"/workflow/get/{uid}",
		GetWorkflow,
	},
	Route{
		"DeleteWorkflow",
		"DELETE",
		"/workflow/delete/{uid}",
		DeleteWorkflow,
	},
	Route{
		"ModifyWorkflow",
		"POST",
		"/workflow/modify/{uid}",
		ModifyWorkflow,
	},
	Route{
		"DeployedWorkflow",
		"Get",
		"/workflow/deployed/{uid}",
		DeployedWorkflow,
	},

	Route{
		"GetDeployedWorkflows",
		"GET",
		"/workflow/get/deployed",
		GetDeployedWorkflows,
	},
	Route{
		"GetAllWorkflows",
		"GET",
		"/workflow/get/all",
		GetAllWorkflows,
	},
	Route{
		"GetHello",
		"GET",
		"/getHello",
		GetHello,
	},
	Route{
		"WriteServiceMetadata",
		"POST",
		"/service/metadata",
		WriteServiceMetadata,
	},
	Route{
		"GetServicesMetadata",
		"GET",
		"/service/metadata",
		GetServicesMetadata,
	},
	Route{
		"WriteConcreteMap",
		"POST",
		"/join/write",
		WriteConcreteMap,
	},
	Route{
		"GetConcreteMap",
		"GET",
		"/join/get/{uid}",
		GetConcreteMap,
	},
}
