package main

import (
	"encoding/json"
	_ "fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Ricezione abstract workflow
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Endpoint http for a user who send an abstract workflow and store it in MongoDB
 */
func ReceiveAbstractWorkflow(w http.ResponseWriter, r *http.Request) {

	var workflowAbstract WorkflowAbstract
	body, _ := ioutil.ReadAll(r.Body)

	if err := json.Unmarshal(body, &workflowAbstract); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)

	}
	workflowAbstract.Created = time.Now().Format(time.RFC850)
	WriteWorkFlowMetadata(workflowAbstract)

	//Posso scrivere questo stesso workflow anche in Neo4j (probabilmente non va bene metterlo in cascata)
	//WriteWorkFlowGraph(workflow)

	//potrei recuperare l'id dal record appena creato, e scriverlo sulla coda di uno dei nodi..

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
}

/**
 * Endpoint for Echo Test
 */
func GetHello(w http.ResponseWriter, r *http.Request) {
	log.Printf("GetHello")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

/**
 * Endpoint for a user who want to send the signal that can start the composition
 */
func DeployWorkflow(w http.ResponseWriter, r *http.Request) {

	var deployTask DeployTask
	body, _ := ioutil.ReadAll(r.Body)

	if err := json.Unmarshal(body, &deployTask); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
	deployTask.Created = time.Now().Format(time.RFC850)
	DeployWorkFlowToNode(deployTask)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

/**
 * Endpoint to retrieve the info about an abstract workflow from MongoDB
 */
func GetWorkflow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid := vars["uid"]
	workflowAbstract := GetWorkFlowMetadata(uid)

	log.Printf("Get")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workflowAbstract); err != nil {
		panic(err)
	}
}

/**
 * Endpoint to delete the info about an abstract workflow from MongoDB
 */
func DeleteWorkflow(w http.ResponseWriter, r *http.Request) {

	log.Printf("Delete")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNoContent)
}

/**
 * Endpoint to modify the info about an abstract workflow from MongoDB
 */
func ModifyWorkflow(w http.ResponseWriter, r *http.Request) {

	log.Printf("Modify")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func DeployedWorkflow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid := vars["uid"]
	log.Printf("Is Deployed ")
	UpdateWorkflowDeployed(uid)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func GetDeployedWorkflows(w http.ResponseWriter, r *http.Request) {
	workflowAbstractList := GetDeployedWorkFlowAbstractList()
	log.Printf("Get Deployed Workflow")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workflowAbstractList); err != nil {
		panic(err)
	}
}

func GetAllWorkflows(w http.ResponseWriter, r *http.Request) {
	workflowAbstractList := GetAllWorkFlowAbstractList()
	log.Printf("Get All Workflow")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workflowAbstractList); err != nil {
		panic(err)
	}
}

//******************************************************************************************

func WriteServiceMetadata(w http.ResponseWriter, r *http.Request) {
	var metaServices MetaServices
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &metaServices); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
	for _, metaService := range metaServices {

		metaService.Created = time.Now().Format(time.RFC850)
		WriteMetadata(metaService)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

}
func GetServicesMetadata(w http.ResponseWriter, r *http.Request) {

	result := GetAllMetadata()
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}
}

//***********************************************************************************************

func WriteConcreteMap(w http.ResponseWriter, r *http.Request) {
	var concreteMap ConcreteMap
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &concreteMap); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
	//metaService.Created = time.Now().Format(time.RFC850)
	WriteJoin(concreteMap)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

}

func GetConcreteMap(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uid := vars["uid"]
	concreteMap := GetJoin(uid)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(concreteMap); err != nil {
		panic(err)
	}
}
