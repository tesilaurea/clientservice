package main

type MetaService struct {
	Uid          string  `json:"uid"` //uid scelto dall'utente, potrebbe non essere univoco (rivedere questo aspetto)
	Type         string  `json:"type"`
	Created      string  `json:"created"`
	Author       string  `json:"author"`
	Ip           string  `json:"ip"`
	Node         string  `json:"node"`
	Availability float64 `json:"availability"`
	Speedup      float64 `json:"speedup"`
}

type MetaServices []MetaService
