package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2/bson"
	_ "log"
)

func WriteWorkFlowMetadata(workflowAbstract WorkflowAbstract) {

	//workflow.Created = RandLetterNumberRunes(32)
	workflowAbstract.Author = "falberto" //*****************************
	err := collection.Insert(&workflowAbstract)
	if err != nil {
		panic(err)
	}
}

/*
// posso fare di meglio su Neo4j ????
func WriteWorkFlowGraph(workflowAbstract WorkflowAbstract) {

	for i, _ := range workflowAbstract.Services {

		v := workflowAbstract.Services[i]
		// Start by creating a node
		result, err := conn.ExecNeo("CREATE (s:SERVICE"+v.Name+" {name: {name}, type: {type}})",
			map[string]interface{}{"name": v.Name, "type": v.Type})

		if result == nil {
			panic(err)
		}
		numResult, _ := result.RowsAffected()
		log.Printf("CREATED ROWS: %d\n", numResult) // CREATED ROWS: 1

	}

	for j, _ := range workflowAbstract.Connections {
		r := workflowAbstract.Connections[j]
		conn.ExecPipeline([]string{
			"MATCH (a {name:" + "\"" + r.From.Name + "\"" + "}), (b {name: " + "\"" + r.To.Name + "\"" + "}) create (a)-[:RELATED]->(b) RETURN a,b",
		}, nil)
		log.Printf("MATCH (a {name:" + "\"" + r.From.Name + "\"" + "}), (b {name: " + "\"" + r.To.Name + "\"" + "}) create (a)-[:RELATED]->(b) RETURN a,b")
	}
}
*/
//Vedere se posso fare di meglio
func DeployWorkFlowToNode(deployTask DeployTask) {
	body := JsonToString(StructToJson(deployTask))
	err := ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	if err != nil {
		panic(err)
	}
}

func GetWorkFlowMetadata(uid string) WorkflowAbstract {
	result := WorkflowAbstract{}
	err := collection.Find(bson.M{"uid": uid}).One(&result)
	if err != nil {
		panic(err)
	}
	return result
}

func GetDeployedWorkFlowAbstractList() []WorkflowAbstract {
	var result []WorkflowAbstract
	err := collection.Find(bson.M{"isdeploy": true}).All(&result)
	if err != nil {
		panic(err)
	}
	return result
}

func GetAllWorkFlowAbstractList() []WorkflowAbstract {
	var result []WorkflowAbstract
	err := collection.Find(bson.M{}).All(&result)
	if err != nil {
		panic(err)
	}
	return result

}
func WriteMetadata(metaService MetaService) {

	metaService.Author = "falberto"
	result := MetaService{}

	err := collectionMeta.Find(bson.M{"uid": metaService.Uid}).One(&result)
	if err != nil {
		err2 := collectionMeta.Insert(&metaService)
		if err2 != nil {
			panic(err2)
		}
	}
}

func GetAllMetadata() MetaServices {
	var result MetaServices
	err := collectionMeta.Find(bson.M{}).All(&result)
	if err != nil {
		panic(err)
	}
	return result
}

// n.b. per come è definito tutte le volte che faccio il deploy non deployo un'altra volta
// lo stesso workflow, ma sovrascrivo quello precedente. Per avere due copie dallo stesso template
// quando creo il workflow o faccio il deploy passare un diverso uid
// tirali fuori tutti, se è presente lo aggiorni, altrimenti lo inserisci
func WriteJoin(concreteMap ConcreteMap) {
	result := ConcreteMap{}

	/*err := collectionJoin.Insert(&concreteMap)
	if err != nil {
		panic(err)
	}*/
	err := collectionJoin.Find(bson.M{"uid": concreteMap.Uid}).One(&result)
	if err != nil {
		err2 := collectionJoin.Insert(&concreteMap)
		if err2 != nil {
			//panic(err2)
			fmt.Println("Errore nella insert di ConcreteMap " + concreteMap.Uid)
		}
		return
	}
	// Update
	colQuerier := bson.M{"uid": concreteMap.Uid}
	change := bson.M{"$set": bson.M{
		"services": concreteMap.Services,
		"join":     concreteMap.Joines,
		"tmax":     concreteMap.TMax,
		"tmin":     concreteMap.TMin,
		"logamax":  concreteMap.LogAMax,
		"logamin":  concreteMap.LogAMin,
		"r":        concreteMap.R,
		"loga":     concreteMap.LogA,
	}}
	err2 := collection.Update(colQuerier, change)
	if err2 != nil {
		//panic(err2)
		fmt.Println("Errore nella update di ConcreteMap " + concreteMap.Uid)
	}

}

func GetJoin(uid string) ConcreteMap {
	var result ConcreteMap
	err := collectionJoin.Find(bson.M{"uid": uid}).One(&result)
	if err != nil {
		panic(err)
	}
	return result
}

func UpdateWorkflowDeployed(uid string) {
	// Update
	colQuerier := bson.M{"uid": uid}
	change := bson.M{"$set": bson.M{"isdeploy": true}}
	err := collection.Update(colQuerier, change)
	if err != nil {
		panic(err)
	}
}
