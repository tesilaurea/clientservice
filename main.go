package main

import (
	"fmt"
	"github.com/gorilla/handlers"
	_ "github.com/johnnadratowski/golang-neo4j-bolt-driver"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2"
	"log"
	"net/http"
	"os"
)

/**
 * Main principale del client backend scritto in Go
 */

func main() {

	port := "8081"
	//amqpPath := "amqp://guest:guest@localhost:5672/"
	amqpPath = "amqp://guest:guest@" + os.Getenv("RABBITMQ_HOST") + ":5672/"
	rabbitMQUrl = amqpPath
	//documentDBUrl = = "mongodb://falberto89:gqm3242@+"+ ds137435.mlab.com + ":37435/tesi"
	documentDBUrl = "mongodb://falberto89:gqm3242@" + os.Getenv("MONGODB_HOST") + ":27017/tesi"
	fmt.Println(documentDBUrl)
	rabbitMQTopic := "deploy"
	mongoCollectionWorkflow := "workflow"
	mongoCollectionService := "service"
	mongoCollectionJoin := "join"
	mongoDB := "tesi"

	//Mi interfaccio con MongoDB
	session, err := mgo.Dial(documentDBUrl)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	collection = session.DB(mongoDB).C(mongoCollectionWorkflow)
	collectionMeta = session.DB(mongoDB).C(mongoCollectionService)
	collectionJoin = session.DB(mongoDB).C(mongoCollectionJoin)
	//Mi interfaccio con Neo4j
	//driver = golangNeo4jBoltDriver.NewDriver()
	//conn, _ = driver.OpenNeo(graphDBUrl)

	//defer conn.Close()

	//Mi interfaccio a RabbitMQ
	mom, err = amqp.Dial(amqpPath)
	if err != nil {
		panic(err)
	}

	defer mom.Close()
	ch, err = mom.Channel()
	if err != nil {
		panic(err)
	}

	defer ch.Close()

	q, err = ch.QueueDeclare(
		rabbitMQTopic, // name
		false,         // durable
		false,         // delete when unused
		false,         // exclusive
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		panic(err)
	}
	router := NewRouter()
	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS()(router)))

}
